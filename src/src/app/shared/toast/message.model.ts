﻿export class Message {
  constructor(
    public type: number,
    public text: string,
    public autoClose: boolean = true,
    public callback?: any
  ) {}
}
