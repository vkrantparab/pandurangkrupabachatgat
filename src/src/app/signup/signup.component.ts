import { Component, OnInit } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { UserService } from "../_services/user.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  signupFormGroup: FormGroup;
  constructor(
    private _userService: UserService,
    private _router: Router,
    private _toaster: ToastrService
  ) {}

  ngOnInit() {
    this.signupFormGroup = new FormGroup({
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl("", Validators.required),
      email: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required),
      confirmPassword: new FormControl("", Validators.required)
    });
  }

  onFormSubmit() {
    this.blockUI.start("Loading");
    let signupData = { ...this.signupFormGroup.value, handle: "users" };
    this._userService.signUp(signupData).subscribe(
      (res: any) => {
        this._toaster.success("User signedup successfully");
        this.blockUI.stop();
        this._router.navigate(["dashboard"]);
      },
      err => {
        this.blockUI.stop();
        this._toaster.error(err.error.error.message);
        console.log(err.error.error.message);
      }
    );
  }
}
