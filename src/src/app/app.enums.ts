export namespace Enums {
  export enum MessageType {
    Success = 1,
    Error = 2,
    Warn = 3,
    Info = 4
  }
}
