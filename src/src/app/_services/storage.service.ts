﻿import { Injectable } from "@angular/core";

@Injectable()
export class StorageService {
  private static keys = new Array<string>();
  private static vals = new Array<any>();

  public static get count(): number {
    return StorageService.keys.length;
  }

  // Set an item (same as native StorageService object).
  public static setItem(key: string, value: any): number {
    var index: number = StorageService.keys.indexOf(key);
    if (index != -1) {
      // Update (overwrite) existing:
      StorageService.vals[index] = value; //.toString();
    } else {
      // Create new:
      index = StorageService.keys.length;
      StorageService.keys.push(key);
      StorageService.vals.push(value);
    }
    return index; // Return index for created/updated item.
  }

  // Get an item (value at key; same as native StorageService object).
  public static getItem(key: string): any {
    var index: number = StorageService.keys.indexOf(key);
    var result: any = null;
    if (index != -1) {
      result = StorageService.vals[index];
    }
    return result;
  }

  // Remove an item (same as native StorageService object).
  public static removeItem(key: string): void {
    var index: number = StorageService.keys.indexOf(key);
    if (StorageService.keys.indexOf(key) != -1) {
      StorageService.keys.splice(index, 1);
      StorageService.vals.splice(index, 1);
    }
  }

  // Clear all stored items (same as native StorageService object).
  public static clear(): void {
    StorageService.keys = [];
    StorageService.vals = [];
  }

  // Get an item key at specified index (same as native StorageService object).
  public static key(index: number): string {
    var result: string = "";
    if (StorageService.keys[index] != null) {
      result = StorageService.keys[index];
    }
    return result;
  }

  // Return all items in storage.
  // AFAIK native StorageService does not do this;
  // so really just a convience method.
  public static getItems(): string[] {
    var pairs = new Array<string>();
    for (var i: number = 0; i < StorageService.keys.length; i++) {
      pairs.push(
        StorageService.keys[i] + "=>" + String(StorageService.vals[i])
      );
    }
    return pairs;
  }
}
