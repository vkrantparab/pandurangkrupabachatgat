import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Urls } from "../app.urls";

@Injectable({
  providedIn: "root"
})
export class UserService {
  constructor(private _httpService: HttpClient) {}

  authenticate(userCreds: any) {
    return this._httpService.post(Urls.Users.Login, userCreds);
  }

  signUp(userData: any) {
    return this._httpService.post(Urls.Users.SignUp, userData);
  }
}
