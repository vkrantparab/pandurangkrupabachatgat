import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Urls } from "../app.urls";

@Injectable({
  providedIn: "root"
})
export class EntriesService {
  constructor(private _httpService: HttpClient) {}

  getEntries() {
    return this._httpService.get(Urls.Entry.Entries, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`
      }
    });
  }

  postEntry(userData: any) {
    return this._httpService.post(Urls.Users.SignUp, userData);
  }
}
