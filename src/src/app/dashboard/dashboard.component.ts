import { Component, OnInit } from "@angular/core";
import { StorageService } from "../_services/storage.service";
import { HttpClient } from "@angular/common/http";
import { EntriesService } from "../_services/entries.service";
import { ToastrService } from "ngx-toastr";
import { Message } from "../shared/toast/message.model";
import { Enums } from "../app.enums";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  constructor(
    private _entriesService: EntriesService,
    private _toasterService: ToastrService
  ) {}

  ngOnInit() {
    console.log(localStorage.getItem("accessToken"));
    this._entriesService.getEntries().subscribe(
      res => {
        this._toasterService.success("Entries loaded successfully");
      },
      err => {
        this._toasterService.error("Failed to load entries");
      }
    );
  }
}
