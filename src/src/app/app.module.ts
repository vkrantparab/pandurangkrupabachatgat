import { BrowserModule } from "@angular/platform-browser";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DemoMaterialModule } from "./material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppHeaderComponent } from "./app-header/app-header.component";
import { AppBodyComponent } from "./app-body/app-body.component";
import { UserService } from "./_services/user.service";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { BlockUIModule } from "ng-block-ui";
import { RouterModule, PreloadAllModules } from "@angular/router";
import { appRoutes } from "./app.routes";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { EntriesService } from "./_services/entries.service";
import { ToastrModule } from "ngx-toastr";
import { CommonModule } from "@angular/common";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatRippleModule } from "@angular/material/core";
import { SignupComponent } from "./signup/signup.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AppHeaderComponent,
    AppBodyComponent,
    DashboardComponent,
    SignupComponent
  ],
  imports: [
    BlockUIModule.forRoot(),
    BrowserModule,
    RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules }),
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(), // ToastrModule added
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule
  ],
  exports: [BrowserAnimationsModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [UserService, EntriesService],
  bootstrap: [AppComponent]
})
export class AppModule {}
