import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { UserService } from "../_services/user.service";
import { StorageService } from "../_services/storage.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  loginFormGroup: FormGroup;
  constructor(
    private _userService: UserService,
    private _router: Router,
    private _toaster: ToastrService
  ) {}

  ngOnInit() {
    this.loginFormGroup = new FormGroup({
      email: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required)
    });
  }
  onFormSubmit() {
    this.blockUI.start("Loading");
    this._userService.authenticate(this.loginFormGroup.value).subscribe(
      (res: any) => {
        localStorage.setItem("accessToken", res.token);
        this.blockUI.stop();
        this._router.navigate(["dashboard"]);
      },
      err => {
        this._toaster.error(err.error.general);
        this.blockUI.stop();
      }
    );
  }

  goToSignup() {
    this._router.navigate(["signup"]);
  }
}
