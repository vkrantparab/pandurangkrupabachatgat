import { Routes } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { SignupComponent } from "./signup/signup.component";

export const appRoutes: Routes = [
  {
    path: "home",
    component: LoginComponent,
    data: { state: "callback" }
  },
  {
    path: "signup",
    component: SignupComponent,
    data: { state: "signUp" }
  },
  {
    path: "dashboard",
    component: DashboardComponent,
    data: { state: "dashboard" }
  },
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "**", redirectTo: "home" }
];
