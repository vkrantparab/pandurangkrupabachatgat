export namespace Urls {
  export const BaseURL =
    "https://us-central1-pandurangkrupabachatgat-fce16.cloudfunctions.net/api";

  export class Users {
    static Login = `${BaseURL}/login`;
    static SignUp = `${BaseURL}/signup`;
  }

  export class Entry {
    static Entries = `${BaseURL}/getEntries`;
  }
}
